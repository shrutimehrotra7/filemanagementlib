package com.fc.payments;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
/**
 * Created by shruti.mehrotra on 16/5/16.
 */
@SpringBootApplication
@ImportResource("classpath:spring/application-context.xml")
@ComponentScan({ "com.snapdeal.payments" })
//@EnableAutoConfiguration
@PropertySource("file:${config.path:src/main/resources/conf}/application.properties")
@EnableBatchProcessing
public class FileManagementLibApplication {

   public static void main(String[] args) {
      SpringApplication.run(FileManagementLibApplication.class, args);
      System.out.println("Application started!");
   }

}
