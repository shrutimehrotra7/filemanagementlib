package com.fc.payments.trigger.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.fc.payments.batch.handlers.FileStatusManagementHandler;
import com.fc.payments.exceptions.FileManagementLibException;
import com.fc.payments.exceptions.InvalidFileStatusException;
import com.fc.payments.file.generator.impl.FileGeneratorFactory;
import com.fc.payments.file.snapshot.IFileSnapshotter;
import com.fc.payments.file.snapshot.impl.FileSnapshotUpdateServiceImpl;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.request.ConfigureFileSnapshotRequest;
import com.fc.payments.model.request.FileManagementRequest;
import com.fc.payments.model.type.FileStatus;
import com.fc.payments.util.FileGenUtil;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
@Service("TriggerService")
public class TriggerService {

	@Autowired
	private FileStatusManagementHandler fileStatusManagementHandler;

	@Autowired
	private FileGeneratorFactory fileGenFactory;

	@Autowired
	private FileGenUtil fileGenUtil;

	@Autowired
	private FileSnapshotUpdateServiceImpl snapshotUpdateService;

	@Autowired
	@Qualifier("FileSnapshotterImpl")
	private IFileSnapshotter fileSnapshotter;
	
	public void triggerJob() throws Exception {
		fileStatusManagementHandler.processFile("", "", "", null, "");
	}

	public String triggerFileGeneration(FileManagementRequest request) throws FileManagementLibException {
		FileSnapshotAndConfigResponse response = fileGenUtil.getSnapshotAndConfigByFileId(request.getFileId());
		if (response.getFileSnapshotDto().getFileStatus() != FileStatus.SNAPSHOT) {
			throw new InvalidFileStatusException("Cannot generate file before snapshot step!");
		}
		return fileGenFactory.getFileGeneratorByFileExtn(response.getFileConfigSnapshotDto().getFileExtension())
				.generateFile(request, response);
	}

	public String configureFileSnapshot(ConfigureFileSnapshotRequest request) {
		return snapshotUpdateService.insert(request);
	}

	public void snapshotFile(FileManagementRequest request) {
		String fileId = request.getFileId();
		fileSnapshotter.doSnapshot(fileId);
	}
}
