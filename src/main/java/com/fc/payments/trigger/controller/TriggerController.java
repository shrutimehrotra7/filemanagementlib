package com.fc.payments.trigger.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fc.payments.exceptions.FileManagementLibException;
import com.fc.payments.model.request.ConfigureFileSnapshotRequest;
import com.fc.payments.model.request.FileManagementRequest;
import com.fc.payments.trigger.service.TriggerService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
@RestController
@Slf4j
public class TriggerController {

	@Autowired
	private TriggerService triggerService;

	@RequestMapping(value = "/triggerFileStatusManagementJob", method = RequestMethod.GET)
	public void triggerFileStatusManagementJob() {
		try {
			triggerService.triggerJob();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@RequestMapping(value = "/triggerFileGeneration", method = RequestMethod.POST)
	public String triggerFileGeneration(@RequestBody FileManagementRequest request) {
		try {
			return triggerService.triggerFileGeneration(request);
		} catch (FileManagementLibException e) {
			log.error("Error occured in file generation for the fileId: {}. Error: {}", request.getFileId(), e);
			throw e;
		}
	}
	
	@RequestMapping(value = "/snapshotFile", method = RequestMethod.POST)
	public void snapshotFile(@RequestBody FileManagementRequest request) {
		try {
			triggerService.snapshotFile(request);
		} catch (FileManagementLibException e) {
			log.error("Error occured during file snapshot for the fileId: {}. Error: {}", request.getFileId(), e);
			throw e;
		}
	}
	
	@RequestMapping(value = "/configureFileSnapshot", method = RequestMethod.POST)
	public String configureFileSnapshot(@RequestBody ConfigureFileSnapshotRequest request) {
		try {
			return triggerService.configureFileSnapshot(request);
		} catch (FileManagementLibException e) {
			log.error("Error occured in file configuring file snapshot for the request {}. Error: {}", request.toString(), e);
			throw e;
		}
	}
}
