package com.fc.payments.util;

import java.util.UUID;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */
public class GuidGenerator {
	public static String getTimestampedGuid() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
