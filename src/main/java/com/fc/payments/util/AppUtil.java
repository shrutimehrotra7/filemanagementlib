package com.fc.payments.util;

import org.apache.commons.lang3.StringUtils;

import com.fc.payments.constants.ApplicationConstants;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class AppUtil {
	public static String taggenateString(String subStr) {
		StringBuilder taggedString = new StringBuilder();
		taggedString.append(ApplicationConstants.OPEN_TAG).append(subStr).append(ApplicationConstants.CLOSE_TAG);

		return taggedString.toString();
	}
	
	public static String quoteString(String input) {
		StringBuilder output = new StringBuilder();
		output.append("'")
				.append(input)
				.append("'");
		return output.toString();
	}

	public static String[] getStringsBetweenTags(String input) {
		String[] substringsBetween = StringUtils.substringsBetween(input, ApplicationConstants.OPEN_TAG,
				ApplicationConstants.CLOSE_TAG);
		return substringsBetween;
	}
}
