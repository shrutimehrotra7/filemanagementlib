package com.fc.payments.util;

import com.fc.payments.model.dto.DataStats;
import com.fc.payments.model.dto.DynamicRoutingData;
import com.fc.payments.model.dto.TrendStats;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class TrendFinderUtil {
	
	public static boolean isUptrendingData(double[] input, int inputLen) {
		TrendStats trendStats = findTrendDataForConsecutiveElements(input, inputLen);
		
		return trendStats.getTrendScore() >= 0 ? true:false;
		
	}
	
	public static DynamicRoutingData getDynamicRoutingData(double[] input, int inputLen) {
		DynamicRoutingData routingData = new DynamicRoutingData();
		
		DataStats dataStats = getDataStats(input, input.length);
		double[] normalPoints = new double[input.length];
		double average = dataStats.getAverage();
		double stdDev = dataStats.getStdev();
		int normalIndex = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] <= average + stdDev && input[i] >= average - stdDev) {
				normalPoints[normalIndex++] = input[i];
			}
		}
		TrendStats trendStats = findTrendDataForConsecutiveElements(normalPoints, normalIndex);
		routingData.setTrendStats(trendStats);
		routingData.setUptrending(trendStats.getTrendScore() >= 0 ? true:false);
		routingData.setStandardPercentArr(normalPoints);
		return null;
		
	}

	public static TrendStats findTrendDataForConsecutiveElements(double[] input, int inputLen) {
		int trend = 0;
		double peakValue = input[0];
		double effectivePercentage = 0;
		double avgStdPercentage = 0;
		int peakIndex = 0;
		for (int i = 0; i < inputLen - 1; ++i) {
			double diff = input[i + 1] - input[i];
			if (diff >= 0) {
				trend++;
				effectivePercentage += diff;
			} else if (diff < 0) {
				trend--;
				effectivePercentage -= diff;
			}
			if (input[i] > peakValue) {
				peakValue = input[i];
				peakIndex = i;
			}
			avgStdPercentage += input[i];
		}
		avgStdPercentage /= inputLen;
		return new TrendStats(trend, peakValue, peakIndex, effectivePercentage, avgStdPercentage);
	}

	public static DataStats getDataStats(double[] input, int inputLen) {
		double stdev = 0;

		double powerSum1 = 0;
		double powerSum2 = 0;
		double average = 0;
		int total = 0;
		
		//Calculating standard deviation
		for (int i = 0; i < inputLen; i++) {
			powerSum1 += input[i];
			powerSum2 += Math.pow(input[i], 2);
			double temp = i * powerSum2 - Math.pow(powerSum1, 2);
			if (temp < 0) {
				temp = -temp;
			}
			stdev = Math.sqrt(temp) / i;
			System.out.println(input[i]);
			total += input[i];
		}
		average = total / inputLen;

		double coefficientOfVariation = (stdev / average) * 100;

		double upperbound = average + stdev;
		double lowerbound = average - stdev;

		return new DataStats(stdev, average, coefficientOfVariation, upperbound, lowerbound);
	}
	
	public static void main(String[] args) {
//		double input[] = { 65, 28, 70, 25, 45, 5, 5, 50, 30, 65 };
		double input[] = { 65, 28, 70, 25, 45, 5, 5, 1, 1, 1 };
		double[] pointsMoreThanAveragePlusStdDev = new double[input.length];
		double[] pointsLessThanAverageMinusStdDev = new double[input.length];
		double[] normalPoints = new double[input.length];

		DataStats dataStats = getDataStats(input, input.length);

		double average = dataStats.getAverage();
		double stdDev = dataStats.getStdev();
		int moreIndex = 0, lessIndex = 0, normalIndex = 0;
		for (int i = 0; i < input.length; i++) {
			if (input[i] > average + stdDev) {
				pointsMoreThanAveragePlusStdDev[moreIndex++] = input[i];
			} else if (input[i] < average - stdDev) {
				pointsLessThanAverageMinusStdDev[lessIndex++] = input[i];
			} else {
				normalPoints[normalIndex++] = input[i];
			}
		}
		System.out.println("Actual data stats: " + dataStats.toString());

		DataStats dataStatsOfLargerNos = getDataStats(pointsMoreThanAveragePlusStdDev, moreIndex);
		System.out.println("Data stats of larger numbers: " + dataStatsOfLargerNos.toString());

		DataStats dataStatsOfSmallerNos = getDataStats(pointsLessThanAverageMinusStdDev, lessIndex);
		System.out.println("Data stats of smaller numbers: " + dataStatsOfSmallerNos.toString());

		DataStats dataStatsOfNormalNos = getDataStats(normalPoints, normalIndex);
		System.out.println("Data stats of Normal numbers: " + dataStatsOfNormalNos.toString());

		System.out.println("Is uptrending:" + isUptrendingData(normalPoints, normalIndex));
		
		System.out.println("Dynamic Routing stats: " + getDynamicRoutingData(input, input.length));
	}

}
