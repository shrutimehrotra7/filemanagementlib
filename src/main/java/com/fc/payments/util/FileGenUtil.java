package com.fc.payments.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fc.payments.constants.ApplicationConstants;
import com.fc.payments.constants.DBConstants;
import com.fc.payments.file.snapshot.IFileSnapshotFetcherService;
import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.dto.FileConfigSnapshotDto;
import com.fc.payments.model.dto.FileObject;
import com.fc.payments.model.type.FileConfigType;
import com.fc.payments.model.type.FileExtension;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Component
public class FileGenUtil {
	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;
	
	public FileSnapshotAndConfigResponse getSnapshotAndConfigByFileId(String fileId) {
		return snapshotFetcherService.getFileStateAndConfigByFileId(fileId);
	}

	public FileTemplate getFileTemplateObjFromStr(String fileTemplate, String fileId) {
		return snapshotFetcherService.getFileTemplateObjFromStr(fileTemplate, fileId);
	}
	
	public String getCompleteFileName(FileObject fileObj) {
		StringBuilder fileUrl = new StringBuilder();
		fileUrl.append(fileObj.getFilePath());
		fileUrl.append(fileObj.getFileName());
		return fileUrl.toString();
	}
	
	public String getValidFilePath(String filePath) {
		if (!(filePath.endsWith(ApplicationConstants.FORWARD_SLASH) ||
				filePath.endsWith(ApplicationConstants.BACKWARD_SLASH))) {
			filePath += ApplicationConstants.FORWARD_SLASH;
		}
		
		return filePath;
	}
	
	public FileObject getFileObjectForRegexDto(FileConfigSnapshotDto dto) {
		dto = resolveRegexInDto(dto);
		String fileName = dto.getFileNameRegex();
		String filePath = getValidFilePath(dto.getFilePathRegex());
		return new FileObject(filePath, fileName);
	}

	public static FileConfigSnapshotDto resolveRegexInDto(FileConfigSnapshotDto fileConfigDto) {

		FileConfigSnapshotDto finalDto = fileConfigDto;

		finalDto.setFilePathRegex(resolveRegexInString(fileConfigDto.getFilePathRegex(), fileConfigDto));
		StringBuilder absFileName = new StringBuilder(
				resolveRegexInString(fileConfigDto.getFileNameRegex(), fileConfigDto));
		absFileName.append(".").append(fileConfigDto.getFileExtension().getExtension());
		finalDto.setFileNameRegex(absFileName.toString());

		return finalDto;
	}

	public static String resolveRegexInString(String input, FileConfigSnapshotDto fileConfigDto) {
		List<String> allowedRegexList = getAllowedRegexList();
		Map<String, String> allowedRegexMap = getAllowedRegexMap(fileConfigDto);
		String[] substringsBetween = AppUtil.getStringsBetweenTags(input);

		for (String subStr : substringsBetween) {
			if (allowedRegexList.contains(subStr)) {
				input = StringUtils.replace(input, AppUtil.taggenateString(subStr), allowedRegexMap.get(subStr));
			}
		}

		return input;
	}

	public static Map<String, String> getDateRegexMap() {
		List<String> datePatternList = getDateFormatList();

		Map<String, String> dateRegexMap = new HashMap<>();
		Date date = new Date();

		for (String datePattern : datePatternList) {
			SimpleDateFormat formatter = new SimpleDateFormat(datePattern);
			dateRegexMap.put(datePattern, formatter.format(date));
		}

		return dateRegexMap;
	}

	public static List<String> getAllowedRegexList() {
		List<String> dateFormatList = getDateFormatList();
		List<String> allowedRegexList = new ArrayList<>(dateFormatList);
		allowedRegexList.add(DBConstants.FILE_TYPE);
		return allowedRegexList;
	}

	public static Map<String, String> getAllowedRegexMap(FileConfigSnapshotDto dto) {
		Map<String, String> allowedRegexMap = getDateRegexMap();
		allowedRegexMap.put("FILE_TYPE", dto.getFileConfigType().name());
		return allowedRegexMap;
	}

	public static List<String> getDateFormatList() {
		List<String> dateFormatList = Arrays.asList("ddMMyyyy", "MM/dd/yyyy", "dd-M-yyyy hh:mm:ss", "dd MMMM yyyy",
				"dd MMMM yyyy zzzz", "E, dd MMM yyyy HH:mm:ss z", "ddMMyyyyhhmmss");
		return dateFormatList;
	}

	public static void main(String[] args) {
		FileConfigSnapshotDto fileConfigDto = new FileConfigSnapshotDto();
		fileConfigDto.setFilePathRegex("conf/<FILE_TYPE>/<ddMMyyyy>/");
		fileConfigDto.setFileNameRegex("<MM/dd/yyyy>refund");
		fileConfigDto.setFileExtension(FileExtension.CSV);
		fileConfigDto.setFileConfigType(FileConfigType.REFUND_HDFC);
		System.out.println(resolveRegexInDto(fileConfigDto));

	}
}
