package com.fc.payments.model;

import com.fc.payments.model.dto.FileConfigSnapshotDto;
import com.fc.payments.model.dto.FileSnapshotDto;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileSnapshotAndConfigResponse {
	private FileSnapshotDto fileSnapshotDto;
	private FileConfigSnapshotDto fileConfigSnapshotDto;
}
