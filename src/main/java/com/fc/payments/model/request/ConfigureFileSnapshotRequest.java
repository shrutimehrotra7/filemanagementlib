package com.fc.payments.model.request;

import com.fc.payments.dao.entity.FileConfigSnapshotEntity;
import com.fc.payments.dao.entity.FileSnapshotEntity;
import com.fc.payments.model.type.DestinationType;
import com.fc.payments.model.type.FileConfigType;
import com.fc.payments.model.type.FileExtension;
import com.fc.payments.model.type.FileGenType;
import com.fc.payments.model.type.FileStatus;
import com.fc.payments.util.GuidGenerator;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class ConfigureFileSnapshotRequest {
	private FileConfigType fileConfigType;
	private String fileTemplate;
	private String filePathRegex;
	private String fileNameRegex;
	private FileExtension fileExtension;
	private FileGenType fileGenType;
	private DestinationType destinationType;
	private String fileConfigId;
	private String destinationConfig;

	public FileSnapshotEntity toFileSnapshotEntity() {
		FileSnapshotEntity entity = new FileSnapshotEntity();
		entity.setFileId(GuidGenerator.getTimestampedGuid());
		entity.setFileConfigId(this.fileConfigId);
		entity.setFileStatus(FileStatus.INIT.name());
		return entity;
	}

	public FileConfigSnapshotEntity toFileConfigSnapshotEntity() {
		FileConfigSnapshotEntity entity = new FileConfigSnapshotEntity();
		String fileConfigId = GuidGenerator.getTimestampedGuid();
		this.fileConfigId = fileConfigId;
		entity.setFileConfigId(this.fileConfigId);
		entity.setDestinationConfig(this.destinationConfig);
		entity.setDestinationType(destinationType.name());
		entity.setFileConfigType(fileConfigType.name());
		entity.setFileExtension(fileExtension.name());
		entity.setFileGenType(fileGenType.name());
		entity.setFileNameRegex(fileNameRegex);
		entity.setFilePathRegex(filePathRegex);
		entity.setFileTemplate(fileTemplate);
		return entity;
	}
}
