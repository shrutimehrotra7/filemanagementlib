package com.fc.payments.model.request;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileManagementRequest {
	private String fileId;
}
