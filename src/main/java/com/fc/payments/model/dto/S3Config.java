package com.fc.payments.model.dto;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class S3Config {
	private String bucketName;
	private String s3AccessKey;
	private String s3SecretKey;
}
