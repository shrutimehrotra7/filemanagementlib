package com.fc.payments.model.dto;

import com.fc.payments.dao.entity.FileConfigSnapshotEntity;
import com.fc.payments.model.type.DestinationType;
import com.fc.payments.model.type.FileConfigType;
import com.fc.payments.model.type.FileExtension;
import com.fc.payments.model.type.FileGenType;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileConfigSnapshotDto {
	private String fileConfigId;
	private FileConfigType fileConfigType;
	private String fileTemplate;
	private String filePathRegex;
	private String fileNameRegex;
	private FileExtension fileExtension;
	private FileGenType fileGenType;
	private DestinationType destinationType;
	private String destinationConfig;
	private String createTS;
	private String updateTS;

	public FileConfigSnapshotEntity toEntity() {
		FileConfigSnapshotEntity entity = new FileConfigSnapshotEntity();
		entity.setFileConfigId(this.fileConfigId);
		entity.setFileConfigType(this.fileConfigType.toString());
		entity.setFileTemplate(this.fileTemplate);
		entity.setFilePathRegex(this.filePathRegex);
		entity.setFileNameRegex(this.fileNameRegex);
		entity.setFileExtension(this.fileExtension.name());
		entity.setFileGenType(this.fileGenType.toString());
		entity.setDestinationType(this.destinationType.toString());
		entity.setDestinationConfig(this.destinationConfig);
		entity.setCreationTS(this.createTS);
		entity.setUpdateTS(this.updateTS);
		return entity;
	}
}
