package com.fc.payments.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
@AllArgsConstructor
public class FileObject {
	private String filePath;
	private String fileName;
}
