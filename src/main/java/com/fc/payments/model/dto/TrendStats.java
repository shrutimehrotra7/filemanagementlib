package com.fc.payments.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@AllArgsConstructor
public class TrendStats {
	private Integer trendScore;
	private double peak;
	private int peakIndex;
	private double effectivePercentage;
	private double averageStdPercentage;
}
