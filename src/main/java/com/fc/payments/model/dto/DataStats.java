package com.fc.payments.model.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Getter
@Setter
@AllArgsConstructor
@ToString
public class DataStats {
	private double stdev;
	private double average;
	private double coefficientOfVariation;
	private double upperbound;
	private double lowerbound;
}
