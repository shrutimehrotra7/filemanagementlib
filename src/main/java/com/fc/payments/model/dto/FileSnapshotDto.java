package com.fc.payments.model.dto;

import com.fc.payments.dao.entity.FileSnapshotEntity;
import com.fc.payments.model.type.FileStatus;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileSnapshotDto {
	private String fileId;
	private String fileName;
	private String creationTS;
	private String updateTS;
	private FileStatus fileStatus;
	private String fileConfigId;

	public FileSnapshotEntity toEntity() {
		FileSnapshotEntity snapshotEntity = new FileSnapshotEntity();
		snapshotEntity.setFileId(this.fileId);
		snapshotEntity.setFileName(this.fileName);
		snapshotEntity.setFileStatus(this.fileStatus.toString());
		snapshotEntity.setCreationTS(this.creationTS);
		snapshotEntity.setUpdateTS(this.updateTS);
		snapshotEntity.setFileConfigId(this.fileConfigId);
		return snapshotEntity;
	}
}
