package com.fc.payments.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileGenType {
	INBOUND, OUTBOUND;
}
