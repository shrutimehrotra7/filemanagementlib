package com.fc.payments.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileConfigType {
	REFUND_ICICI, REFUND_HDFC;
}
