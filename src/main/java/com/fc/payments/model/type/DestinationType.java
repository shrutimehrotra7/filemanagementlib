package com.fc.payments.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum DestinationType {
	S3, FTP;
}
