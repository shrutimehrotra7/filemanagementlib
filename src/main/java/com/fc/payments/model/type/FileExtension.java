package com.fc.payments.model.type;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileExtension {
	CSV("csv"), XLS("xls");
	
	private FileExtension(String extension) {
		this.extension = extension;
	}
	
	@Getter
	@Setter
	private String extension;
}
