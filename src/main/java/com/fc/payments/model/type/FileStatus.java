package com.fc.payments.model.type;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum FileStatus {
	INIT, SNAPSHOT, GENERATED, UPLOADED, MAILED;
}
