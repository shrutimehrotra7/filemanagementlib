package com.fc.payments.constants;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public class ApplicationConstants {
	public static final String FORWARD_SLASH = "/";
	public static final String BACKWARD_SLASH = "\\";
	public static final String DOT = ".";
	public static final String OPEN_TAG = "<";
	public static final String CLOSE_TAG = ">";
	public static final char SPACE_CHAR = '\u0000';
}
