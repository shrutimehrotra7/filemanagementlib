package com.fc.payments.constants;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com) 
 * created on 17-Mar-2017
 */
public class BatchConstants {
	public static final String FILE_NAME = "fileName";
	
	public static final String LOCAL_FILE_PATH = "localFilePath";
	
	public static final String JOB_FAIL_ATTEMPT_SEQ = "failAttemptSeq";

	public static final String JOB_NAME_FILE_STATUS_MANAGEMENT = "FileStatusMgmt";
}
