package com.fc.payments.constants;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class DBConstants {
	public static final String FILE_ID = "FILE_ID";
	public static final String FILE_STATUS = "FILE_STATUS";
	public static final String FILE_TYPE = "FILE_TYPE";
}
