package com.fc.payments.file.template.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fc.payments.dao.dto.DBQueryDto;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class FileTemplate {
	@NotNull
	private DBConfig dbConfig;
	
	private Header fileHeader = new Header(ColumnType.HEADER.FILE);
	
	@NotNull
	@NotEmpty
	private Batch[] batches;
	
	private char fileDelimiter;
	
	private String validatorClassName;
	
	public String[] columnToStringArray(Column[] colArr) {
		if (colArr != null && colArr.length > 0) {
			String[] colStrArray = new String[colArr.length];
			
			for (int i = 0; i < colStrArray.length; i++) {
				colStrArray[i] = colArr[i].toString();
			}
			
			return colStrArray;
		}
		return null;
	}
	
	public DBQueryDto[] toDBRetrievalDto() {
		DBQueryDto[] dtoArray = new DBQueryDto[batches.length];
		DBQueryDto dto = new DBQueryDto();
		dto.setDbName(this.dbConfig.getDbName());
		dto.setHost(this.dbConfig.getHost());
		dto.setUserName(this.dbConfig.getUserName());
		dto.setPassword(this.dbConfig.getPassword());
		for (int i = 0; i < this.batches.length; i++) {
			if (batches[i].getBody() != null) {
				dto.setColumns(columnToStringArray(batches[i].getBody().getColumns()));
				dto.setTableName(batches[i].getBody().getTableName());
				dto.setWhereClause(batches[i].getBody().getWhereClause());
				dto.setBatchHeader(batches[i].getBatchHeader());
				dto.setBatchFooter(batches[i].getBatchFooter());
				dtoArray[i] = dto;
			}
		}
		return dtoArray;
	}
}
