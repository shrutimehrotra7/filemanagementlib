package com.fc.payments.file.template.dto;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class DBConfig {
	private String dbName;
	private String host;
	private String userName;
	private String password;

}
