package com.fc.payments.file.template.dto;

import com.fc.payments.file.template.dto.ColumnType.HEADER;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class Header {
	private Column[] columns;
	private HEADER type;

	public Header(HEADER header) {
		this.type = header;
	}
}
