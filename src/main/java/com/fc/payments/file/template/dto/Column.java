package com.fc.payments.file.template.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
public class Column {
	@JsonProperty(required = true)
	private String id;
	@JsonProperty(required = true)
	private String name;
	
	@Override
	public String toString() {
		return this.name;
	}
	
}
