package com.fc.payments.file.template.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fc.payments.file.template.dto.ColumnType.FOOTER;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Data
@ToString
public class Footer {
	@JsonProperty(required = true)
	private Column[] columns;
	private FOOTER type;

	public Footer(FOOTER footer) {
		this.type = footer;
	}
}
