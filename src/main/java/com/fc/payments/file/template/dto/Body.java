package com.fc.payments.file.template.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class Body {
	
	@NotNull
	@NotEmpty
	private String[] tableName;
	
	@NotNull
	@NotEmpty
	private String whereClause;
	private Column[] columns;
	private String orderByClause;
}
