package com.fc.payments.file.template.dto;

import javax.validation.constraints.NotNull;

import com.fc.payments.file.template.dto.ColumnType.HEADER;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class Batch {
	private Header batchHeader = new Header(HEADER.BATCH);
	
	@NotNull
	private Body body;
	private Footer batchFooter;

}
