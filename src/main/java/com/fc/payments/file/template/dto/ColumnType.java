package com.fc.payments.file.template.dto;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public enum ColumnType {
	BODY; 
	enum FOOTER{
		COUNT,
		SUM;
	}
	enum HEADER{
		BATCH, 
		FILE;
	}

}
