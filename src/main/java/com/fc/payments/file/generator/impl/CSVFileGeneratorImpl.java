package com.fc.payments.file.generator.impl;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.fc.payments.constants.ApplicationConstants;
import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBQueryResponse;
import com.fc.payments.exceptions.FileGenerationException;
import com.fc.payments.file.generator.BaseFileGeneratorService;
import com.fc.payments.file.generator.IFileGenerator;
import com.fc.payments.file.template.dto.Column;
import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.file.template.dto.Footer;
import com.fc.payments.file.template.dto.Header;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.type.FileExtension;
import com.opencsv.CSVWriter;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("CSVFileGeneratorImpl")
public class CSVFileGeneratorImpl extends BaseFileGeneratorService implements IFileGenerator {

	@Override
	public FileExtension getFileExtension() {
		return FileExtension.CSV;
	}

	@Override
	public boolean createFile(FileTemplate fileTemplate, String fileUrl, FileSnapshotAndConfigResponse response) {
		DBQueryResponse dbResponse = null;
		boolean fileCreated = false;
		// Create CSV file
		FileWriter writer = null;
		CSVWriter csvWriter = null;
		try {
			writer = new FileWriter(fileUrl);

			// using custom delimiter and quote character
			char fileDelimiter = fileTemplate.getFileDelimiter() == ApplicationConstants.SPACE_CHAR ? ',' : fileTemplate.getFileDelimiter();
			csvWriter = new CSVWriter(writer, fileDelimiter);
	
			List<String[]> header = getHeaderArray(fileTemplate.getFileHeader());
	
			DBQueryDto[] dbQueryDtoArray = fileTemplate.toDBRetrievalDto();

			csvWriter.writeAll(header);
			for (DBQueryDto dbQueryDto : dbQueryDtoArray) {
				if (dbQueryDto != null) {
					dbResponse = retrieveBodyByDBQueryDto(dbQueryDto, response);
					csvWriter.writeAll(dbResponse.getRs(), false);
					csvWriter.writeAll(getFooterArray(dbQueryDto.getBatchFooter()));
				}
			}

			fileCreated = true;
		} catch (Exception e) {
			throw new FileGenerationException("Got error while generating file with fileUrl: " + fileUrl, e);
		} finally {
			try {
				if (csvWriter != null) {
					csvWriter.close();
				}
			} catch (IOException e) {
				// cannot do anyhting
			}
			try {
				if (dbResponse.getCon() != null) {
					dbResponse.getCon().close();
				}
			} catch (SQLException se1) {
				/* can't do anything */
			}
			try {
				if (dbResponse.getStmt() != null) {
					dbResponse.getStmt().close();
				}
			} catch (SQLException se1) {
				/* can't do anything */
			}
			try {
				if (dbResponse.getRs() != null) {
					dbResponse.getRs().close();
				}
			} catch (SQLException se) {
				/* can't do anything */
			}
		}

		return fileCreated;

	}

	private List<String[]> getHeaderArray(Header fileHeader) {
		List<String[]> header = new ArrayList<>();
		if (fileHeader != null) {
			String columnNames[] = toStringArray(fileHeader.getColumns());
			if (columnNames != null) {
				header.add(columnNames);
			}
		}
		return header;
	}
	
	private List<String[]> getFooterArray(Footer fileFooter) {
		List<String[]> footer = new ArrayList<>();
		if (fileFooter != null) {
			String[] columnNames = toStringArray(fileFooter.getColumns());
			if (columnNames != null) {
				footer.add(columnNames);
			}
		}
		return footer;
	}
	
	public String[] toStringArray(Column[] columns) {
		if (columns != null && columns.length > 0) {
			String[] columnNames = new String[columns.length];
			for (int i = 0; i < columns.length; i++) {
				columnNames[i] = columns[i].getName();
			}
			return columnNames;
		}
		return null;
	}

}
