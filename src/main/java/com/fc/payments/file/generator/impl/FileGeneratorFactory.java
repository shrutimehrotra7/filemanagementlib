package com.fc.payments.file.generator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fc.payments.file.generator.IFileGenerator;
import com.fc.payments.model.type.FileExtension;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Component
public class FileGeneratorFactory {

	@Autowired
	List<IFileGenerator> fileGenList;

	Map<FileExtension, IFileGenerator> fileGenMap = new HashMap<>();

	@PostConstruct
	public void initBeans() {
		for (IFileGenerator fileGen : fileGenList) {
			if (fileGenMap.containsKey(fileGen.getFileExtension())) {
				throw new RuntimeException(
						"File generator services with duplicate file extension cannot be initialized!");
			}
			fileGenMap.put(fileGen.getFileExtension(), fileGen);
		}
	}
	
	public IFileGenerator getFileGeneratorByFileExtn(FileExtension fileExtension) {
		if (!fileGenMap.containsKey(fileExtension)) {
			throw new RuntimeException("FileGenService: " + fileExtension + " is not registered!");
		}
		return fileGenMap.get(fileExtension);
	}
	
}
