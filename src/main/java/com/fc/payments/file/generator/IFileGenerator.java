package com.fc.payments.file.generator;

import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.request.FileManagementRequest;
import com.fc.payments.model.type.FileExtension;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileGenerator {
	public String generateFile(FileManagementRequest request, FileSnapshotAndConfigResponse response);
	
	public FileExtension getFileExtension();
}
