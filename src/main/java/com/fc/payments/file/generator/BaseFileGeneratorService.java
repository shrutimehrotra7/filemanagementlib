package com.fc.payments.file.generator;

import java.io.File;
import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fc.payments.dao.DBUtil;
import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBQueryResponse;
import com.fc.payments.dao.service.IDBRetrievalService;
import com.fc.payments.exceptions.ExceptionErrorCode;
import com.fc.payments.exceptions.FileGenerationException;
import com.fc.payments.exceptions.InvalidFileConfigException;
import com.fc.payments.file.snapshot.IFileSnapshotUpdateService;
import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.dto.FileObject;
import com.fc.payments.model.request.FileManagementRequest;
import com.fc.payments.model.type.FileStatus;
import com.fc.payments.util.FileGenUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@Component
@Slf4j
public abstract class BaseFileGeneratorService {

	@Autowired
	private FileGenUtil fileGenUtil;

	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotService;

	@Autowired
	@Qualifier("DBRetrievalServiceImpl")
	private IDBRetrievalService retrievalService;

	public abstract boolean createFile(FileTemplate fileTemplate, String fileUrl, FileSnapshotAndConfigResponse response);

	public String generateFile(FileManagementRequest request, FileSnapshotAndConfigResponse response) {
		String fileUrl = null;
		String fileId = request.getFileId();
		// Fetch fileSnapshot and config by fileId
		FileObject fileObj = fileGenUtil.getFileObjectForRegexDto(response.getFileConfigSnapshotDto());
		fileUrl = fileGenUtil.getCompleteFileName(fileObj);

		FileTemplate fileTemplate = fileGenUtil
				.getFileTemplateObjFromStr(response.getFileConfigSnapshotDto().getFileTemplate(), fileId);

		String filePath = fileObj.getFilePath();
		String fileName = fileObj.getFileName();
		File filePathDir = new File(filePath);

		try {
			filePathDir.mkdirs();
			File file = new File(filePathDir, fileName);
			file.createNewFile();
		} catch (IOException e) {
			throw new InvalidFileConfigException(
					"Unable to create file! fileName: " + fileUrl + " ,fileId: " + fileId + " Error: " + e.getMessage(),
					ExceptionErrorCode.INVALID_FILE_REGEX_PATTERN);
		}
		boolean fileCreated = createFile(fileTemplate, fileUrl, response);

		if (!fileCreated) {
			throw new FileGenerationException(
					"Unable to create file with url: " + fileUrl + " for the fileId: " + fileId);
		}

		log.info("File: {} generated for the fileId: {}", fileUrl, fileId);

		// Update file status in the snapshot to GEN
		snapshotService.updateFileUrlAndStatus(FileStatus.GENERATED, fileId, fileUrl);
		log.info("Updated the file status to {}, and fileName to : {} for the fileId: {}", FileStatus.GENERATED,
				fileUrl, fileId);
		return fileUrl;
	}

	public DBQueryResponse retrieveBodyByDBQueryDto(DBQueryDto dbDto, FileSnapshotAndConfigResponse response) {
		dbDto.setWhereClause(DBUtil.replaceParametersInWhereClause(dbDto.getWhereClause(), response));
		String selectQuery = DBUtil.getSelectQueryByDto(dbDto);
		return retrievalService.retrieveData(dbDto, selectQuery);
	}
}
