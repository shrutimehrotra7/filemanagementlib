package com.fc.payments.file;

import com.fc.payments.model.request.FileManagementRequest;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileStatusManagementValidator {
	public boolean isValidRequest(FileManagementRequest request);
}
