package com.fc.payments.file.snapshot.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fc.payments.constants.DBConstants;
import com.fc.payments.dao.DBUtil;
import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBSnapshotterDto;
import com.fc.payments.dao.service.IDBUpdateService;
import com.fc.payments.exceptions.InvalidFileStatusException;
import com.fc.payments.file.snapshot.IFileSnapshotFetcherService;
import com.fc.payments.file.snapshot.IFileSnapshotUpdateService;
import com.fc.payments.file.snapshot.IFileSnapshotter;
import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.model.type.FileStatus;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Slf4j
@Component("FileSnapshotterImpl")
public class FileSnapshotterImpl implements IFileSnapshotter {

	@Autowired
	@Qualifier("FileSnapshotFetcherServiceImpl")
	private IFileSnapshotFetcherService snapshotFetcherService;

	@Autowired
	@Qualifier("DBUpdateServiceImpl")
	private IDBUpdateService dbUpdateService;

	@Autowired
	@Qualifier("FileSnapshotUpdateServiceImpl")
	private IFileSnapshotUpdateService snapshotUpdateService;
	
	@Override
	public void doSnapshot(String fileId) {
		FileTemplate fileTemplate = null;
		// Fetch the snapshot and template info
		FileSnapshotAndConfigResponse response = snapshotFetcherService.getFileStateAndConfigByFileId(fileId);
		String fileTemplateStr = response.getFileConfigSnapshotDto().getFileTemplate();

		if (response.getFileSnapshotDto().getFileStatus() != FileStatus.INIT) {
			throw new InvalidFileStatusException("Cannot generate file before init step!");
		}
		
		fileTemplate = snapshotFetcherService.getFileTemplateObjFromStr(fileTemplateStr, fileId);

		// Array is for different batches in the file template
		for (DBQueryDto dto : fileTemplate.toDBRetrievalDto()) {
			
			if (dto != null) {
				// Initiate snapshot using configuration information
				DBSnapshotterDto snapsotDto = new DBSnapshotterDto(dto);
				Map<String, String> columnToUpdate = new HashMap<>();
				columnToUpdate.put(DBConstants.FILE_ID, fileId);
				snapsotDto.setColumnToUpdate(columnToUpdate);
				snapsotDto.setWhereClause(DBUtil.replaceParametersInWhereClause(snapsotDto.getWhereClause(), response));
				dbUpdateService.update(snapsotDto);
				log.info("Snapshot done successfully for object: {}!", dto.toString());
				snapshotUpdateService.updateFileStatus(FileStatus.SNAPSHOT, fileId);
				log.info("File status updated to {} successfully for the fileId: {}!", FileStatus.SNAPSHOT.name(), fileId);
			}
		}
	}

}
