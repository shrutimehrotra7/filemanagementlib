package com.fc.payments.file.snapshot;
/**
 * 
 * @author shruti.mehrotra
 *
 */
public interface IFileSnapshotter {
	public void doSnapshot(String fileId);
}
