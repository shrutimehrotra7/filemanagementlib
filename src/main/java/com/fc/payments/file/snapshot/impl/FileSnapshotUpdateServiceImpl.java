package com.fc.payments.file.snapshot.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fc.payments.dao.entity.FileConfigSnapshotEntity;
import com.fc.payments.dao.entity.FileSnapshotEntity;
import com.fc.payments.dao.mapper.IFileConfigSnapshotEntityMapper;
import com.fc.payments.dao.mapper.IFileSnapshotEntityMapper;
import com.fc.payments.file.snapshot.IFileSnapshotUpdateService;
import com.fc.payments.model.request.ConfigureFileSnapshotRequest;
import com.fc.payments.model.type.FileStatus;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileSnapshotUpdateServiceImpl")
public class FileSnapshotUpdateServiceImpl implements IFileSnapshotUpdateService {

	@Autowired
	private IFileSnapshotEntityMapper snapshotEntityMapper;
	
	@Autowired
	private IFileConfigSnapshotEntityMapper configSnapshotEntityMapper;

	@Override
	public boolean updateFileStatus(FileStatus fileStatus, String fileId) {
		boolean isSuccess = false;
		FileSnapshotEntity entity = new FileSnapshotEntity();
		entity.setFileId(fileId);
		entity.setFileStatus(fileStatus.name());
		snapshotEntityMapper.updateStatusByFileId(entity);
		return isSuccess;
	}

	@Override
	public boolean updateFileUrlAndStatus(FileStatus fileStatus, String fileId, String fileName) {
		boolean isSuccess = false;
		FileSnapshotEntity entity = new FileSnapshotEntity();
		entity.setFileId(fileId);
		entity.setFileStatus(fileStatus.name());
		entity.setFileName(fileName);
		snapshotEntityMapper.updateFileNameAndStatusByFileId(entity);
		return isSuccess;
	}

	@Override
	public String insert(ConfigureFileSnapshotRequest request) {
		FileConfigSnapshotEntity fileConfigSnapshot = request.toFileConfigSnapshotEntity();
		configSnapshotEntityMapper.insert(fileConfigSnapshot);
		FileSnapshotEntity fileSnapshot = request.toFileSnapshotEntity();
		snapshotEntityMapper.insert(fileSnapshot);
		return fileSnapshot.getFileId();
	}

}
