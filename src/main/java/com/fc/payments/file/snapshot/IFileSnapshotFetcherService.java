package com.fc.payments.file.snapshot;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.model.FileSnapshotAndConfigResponse;

public interface IFileSnapshotFetcherService {
	public FileSnapshotAndConfigResponse getFileStateAndConfigByFileId(String fileId);
	
	public FileTemplate getFileTemplateObjFromStr(String fileTemplateStr, String fileId);
}
