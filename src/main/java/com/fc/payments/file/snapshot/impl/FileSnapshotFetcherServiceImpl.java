package com.fc.payments.file.snapshot.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fc.payments.dao.entity.FileConfigSnapshotEntity;
import com.fc.payments.dao.entity.FileSnapshotEntity;
import com.fc.payments.dao.mapper.IFileConfigSnapshotEntityMapper;
import com.fc.payments.dao.mapper.IFileSnapshotEntityMapper;
import com.fc.payments.exceptions.ExceptionErrorCode;
import com.fc.payments.exceptions.FileConfigNotFoundException;
import com.fc.payments.exceptions.InvalidFileConfigException;
import com.fc.payments.exceptions.InvalidFileIdException;
import com.fc.payments.file.IFileStatusManagementValidator;
import com.fc.payments.file.snapshot.IFileSnapshotFetcherService;
import com.fc.payments.file.template.dto.FileTemplate;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.google.gson.Gson;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("FileSnapshotFetcherServiceImpl")
public class FileSnapshotFetcherServiceImpl implements IFileSnapshotFetcherService {

	@Autowired
	private IFileSnapshotEntityMapper snapshotEntityMapper;

	@Autowired
	private IFileConfigSnapshotEntityMapper configSnapshotEntityMapper;

	@Override
	public FileSnapshotAndConfigResponse getFileStateAndConfigByFileId(String fileId) {
		FileSnapshotAndConfigResponse response = new FileSnapshotAndConfigResponse();
		FileSnapshotEntity snapshotEntity = snapshotEntityMapper.getByFileId(fileId);
		FileConfigSnapshotEntity configSnapshotEntity = null;
		if (snapshotEntity != null) {
			configSnapshotEntity = configSnapshotEntityMapper.getByFileConfigId(snapshotEntity.getFileConfigId());
			response.setFileSnapshotDto(snapshotEntity.toDto());
		} else {
			throw new InvalidFileIdException("Invalid file id: " + fileId);
		}

		if (configSnapshotEntity != null) {
			response.setFileConfigSnapshotDto(configSnapshotEntity.toDto());
		} else {
			throw new FileConfigNotFoundException("No configuration found for the file id: " + fileId
					+ "with configId: " + snapshotEntity.getFileConfigId());
		}

		return response;
	}

	@Override
	public FileTemplate getFileTemplateObjFromStr(String fileTemplateStr, String fileId) {
		FileTemplate fileTemplate = null;
		if (!StringUtils.isEmpty(fileTemplateStr)) {
			fileTemplate = new Gson().fromJson(fileTemplateStr, FileTemplate.class);

		}
		// Check if template is valid..
		if (fileTemplate == null) {
			throw new InvalidFileConfigException("Unparseable file template for file Id: " + fileId,
					ExceptionErrorCode.INVALID_FILE_CONFIG_TEMPLATE);
		}
		
		String validatorClassName = fileTemplate.getValidatorClassName();
		if (!StringUtils.isEmpty(validatorClassName))
			try {
				if (!Class.forName(validatorClassName).isAssignableFrom(IFileStatusManagementValidator.class)) {
					
				}
			} catch (ClassNotFoundException e) {
				throw new InvalidFileConfigException("Invalid validator class in file template for file Id: " + fileId,
						ExceptionErrorCode.INVALID_FILE_CONFIG_TEMPLATE);
			}
		return fileTemplate;
	}

}
