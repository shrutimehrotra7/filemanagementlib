package com.fc.payments.file.snapshot;

import com.fc.payments.model.request.ConfigureFileSnapshotRequest;
import com.fc.payments.model.type.FileStatus;

/**
 * 
 * @author shruti.mehrotra
 *
 */

public interface IFileSnapshotUpdateService {
	public boolean updateFileStatus(FileStatus fileStatus, String fileId);
	
	public boolean updateFileUrlAndStatus(FileStatus fileStatus, String fileId, String fileName);
	
	public String insert(ConfigureFileSnapshotRequest request);
}
