package com.fc.payments.batch.handlers.util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.fc.payments.batch.job.repo.CleanableJobRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author shruti.mehrotra
 * 
 */

@Component
@Slf4j
public class CleanUpJob {
	
	@Autowired
	@Qualifier("MapCleanableJobRepository")
	private CleanableJobRepository cleanUpJobRepo;
	
	public void cleanUpJobData(String jobName) {
		cleanUpJobRepo.clearJobData(jobName);
	}
	
	public void cleanUpPath(String localFilePath) {
		File file = new File(localFilePath);
		if (file.exists()) {
			if (file.isDirectory()) {
				try {
					log.info("Cleaning the contents of the directory: {}", localFilePath);
					FileUtils.cleanDirectory(file);
					log.info("Directory: {} cleaned!", localFilePath);
				} catch (IOException e) {
					throw new RuntimeException(
							"Error While Cleaning up directory: {}"
									+ localFilePath);
				}
			} else {
				log.info("Cleaning the contents of the file: {}", localFilePath);
				if (!new File(localFilePath).delete()) {
					throw new RuntimeException(
							"Error While Cleaning up directory: {}"
									+ localFilePath);
				}
				log.info("File: {} cleaned!", localFilePath);
			}
		}
	}
}
