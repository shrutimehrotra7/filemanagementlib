package com.fc.payments.batch.handlers;

import java.util.Map;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fc.payments.batch.steps.FileCreationStep;
import com.fc.payments.batch.steps.FileIdGenerationStep;
import com.fc.payments.batch.steps.FileSnapshotStep;
import com.fc.payments.batch.steps.FileUploadStep;
import com.fc.payments.batch.steps.SendEmailStep;
import com.fc.payments.constants.BatchConstants;
import com.fc.payments.constants.StepConstant;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */
@Slf4j
@Component
public class FileStatusManagementHandler extends MapBasedCleanableJobHandler {

	@Autowired
	private JobBuilderFactory jobs;

	@Autowired
	private StepBuilderFactory steps;
	
	@Autowired
	private FileIdGenerationStep fileIdGenerationTasklet;

	@Autowired
	private FileSnapshotStep fileSnapshotTasklet;
	
	@Autowired
	private FileCreationStep fileCreationTasklet;
	
	@Autowired
	private FileUploadStep fileUploadTasklet;
	
	@Autowired
	private SendEmailStep sendEmailTasklet;
	
	Map<String, String> jobData = null;
	
	@Override
	public Map<String, String> getJobData() {
		return jobData;
	}

	public String processFile(String filePath, String headerList, 
			String tagName, String domainFtpClientConfig, String updateReason)
			throws Exception {

		String jobName = 
				BatchConstants.JOB_NAME_FILE_STATUS_MANAGEMENT;
		Step fileIdGenerationStep = steps
				.get(StepConstant.getFileIdGenerationStepName(jobName))
				.tasklet(fileIdGenerationTasklet).build();
		Step fileSnapshotStep = steps
				.get(StepConstant.getFileSnapshotStepName(jobName))
				.tasklet(fileSnapshotTasklet).build();
		Step fileCreationStep = steps
				.get(StepConstant.getFileCreationStepName(jobName))
				.tasklet(fileCreationTasklet).build();
		
		Step fileUploaderStep = steps
				.get(StepConstant.getFileUploadStepName(jobName))
				.tasklet(fileUploadTasklet).build();
		
		Step sendEmailStep = steps
				.get(StepConstant.getSendEmailStepName(jobName))
				.tasklet(sendEmailTasklet).build();
		
		Job job = jobs.get(jobName)/*.preventRestart()*/
					  .flow(fileIdGenerationStep)
					  .next(fileSnapshotStep)
					  .next(fileCreationStep)
					  .next(fileUploaderStep)
					  .next(sendEmailStep)
					  .end().build();
					  
		JobParametersBuilder builder = new JobParametersBuilder();
		
		filePath = "testing";
		
		buildJobParameters(builder, filePath, false, headerList, tagName, domainFtpClientConfig, updateReason);
		log.info("Launching job for file status management...");
		launchJob(job, builder);
		
		return null;
	}

	private void buildJobParameters(JobParametersBuilder builder,
			String filePath, Boolean isFirstExecution, 
			String headerList, String tagName,
			String domainFtpClientConfig, String updateReason) {

		builder.addString(BatchConstants.LOCAL_FILE_PATH, filePath);
	}

}
