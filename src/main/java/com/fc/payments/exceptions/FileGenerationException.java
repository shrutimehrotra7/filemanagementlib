package com.fc.payments.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class FileGenerationException extends FileManagementLibException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4844744443924922072L;

	public FileGenerationException() {
	}

	public FileGenerationException(String message) {
		super(message);
	}

	public FileGenerationException(String message, Throwable e) {
		super(message, e);
	}

	public FileGenerationException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.FILE_GEN_ERROR);
	}

}
