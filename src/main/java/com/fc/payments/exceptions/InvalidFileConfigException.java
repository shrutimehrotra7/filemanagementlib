package com.fc.payments.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class InvalidFileConfigException extends FileManagementLibException {



	/**
	 * 
	 */
	private static final long serialVersionUID = -5580445443257244001L;

	public InvalidFileConfigException(ExceptionErrorCode errorCode) {
		setErrorCode(errorCode);
	}

	public InvalidFileConfigException(String message, ExceptionErrorCode errorCode) {
		super(message);
		setErrorCode(errorCode);
	}

	public InvalidFileConfigException(String message, ExceptionErrorCode errorCode, Throwable e) {
		super(message, e);
		setErrorCode(errorCode);
	}

	public InvalidFileConfigException(ExceptionErrorCode errorCode, Throwable e) {
		super(e);
		setErrorCode(errorCode);
	}

}
