package com.fc.payments.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class InvalidFileStatusException extends FileManagementLibException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5998695660395217267L;

	public InvalidFileStatusException() {
	}

	public InvalidFileStatusException(String message) {
		super(message);
	}

	public InvalidFileStatusException(String message, Throwable e) {
		super(message, e);
	}

	public InvalidFileStatusException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.INVALID_FILE_STATUS);
	}

}
