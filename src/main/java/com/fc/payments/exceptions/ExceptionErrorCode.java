package com.fc.payments.exceptions;

/**
 * @author shruti.mehrotra
 *
 */
public enum ExceptionErrorCode {
	INVALID_FILE_ID(100),
	FILE_CONFIG_NOT_FOUND(101), 
	INVALID_FILE_CONFIG(102),
	INVALID_FILE_CONFIG_TEMPLATE(103),
	INVALID_FILE_REGEX_PATTERN(104),
	INVALID_QUERY(105), 
	DB_CONNECT_ERROR(106), 
	FILE_GEN_ERROR(107), 
	INVALID_FILE_STATUS(108); 

   private final Integer errorCode;

   private ExceptionErrorCode(Integer errorCode) {
      this.errorCode = errorCode;
   }

   public Integer getErrorCode() {
      return errorCode;
   }
}
