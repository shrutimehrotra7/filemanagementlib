package com.fc.payments.exceptions;


/**
 * 
 * @author shruti.mehrotra
 * 
 */
public class InvalidFileIdException extends FileManagementLibException {

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8034232969387413816L;

	public InvalidFileIdException() {
	}

	public InvalidFileIdException(String message) {
		super(message);
	}

	public InvalidFileIdException(String message, Throwable e) {
		super(message, e);
	}

	public InvalidFileIdException(Throwable e) {
		super(e);
	}

	{
		setErrorCode(ExceptionErrorCode.INVALID_FILE_ID);
	}

}
