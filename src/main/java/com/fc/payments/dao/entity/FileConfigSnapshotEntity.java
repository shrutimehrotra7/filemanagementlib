package com.fc.payments.dao.entity;

import com.fc.payments.model.dto.FileConfigSnapshotDto;
import com.fc.payments.model.type.DestinationType;
import com.fc.payments.model.type.FileConfigType;
import com.fc.payments.model.type.FileExtension;
import com.fc.payments.model.type.FileGenType;

import lombok.Data;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */

@Data
public class FileConfigSnapshotEntity {
	private String fileConfigId;
	private String fileConfigType;
	private String fileTemplate;
	private String filePathRegex;
	private String fileNameRegex;
	private String fileExtension;
	private String fileGenType;
	private String destinationType;
	private String destinationConfig;
	private String creationTS;
	private String updateTS;
	
	public FileConfigSnapshotDto toDto() {
		FileConfigSnapshotDto dto = new FileConfigSnapshotDto();
		dto.setFileConfigId(this.fileConfigId);
		dto.setFileConfigType(FileConfigType.valueOf(this.fileConfigType));
		dto.setFileTemplate(this.fileTemplate);
		dto.setFilePathRegex(this.filePathRegex);
		dto.setFileNameRegex(this.fileNameRegex);
		dto.setFileExtension(FileExtension.valueOf(this.fileExtension));
		dto.setFileGenType(FileGenType.valueOf(this.fileGenType));
		dto.setDestinationType(DestinationType.valueOf(this.destinationType));
		dto.setDestinationConfig(this.destinationConfig);
		dto.setCreateTS(this.creationTS);
		dto.setUpdateTS(this.updateTS);
		return dto;
	}
}
