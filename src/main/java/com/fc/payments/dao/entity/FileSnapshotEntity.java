package com.fc.payments.dao.entity;

import com.fc.payments.model.dto.FileSnapshotDto;
import com.fc.payments.model.type.FileStatus;

import lombok.Data;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */

@Data
public class FileSnapshotEntity {
	
	private String fileId;
	private String fileName;
	private String fileStatus;
	private String creationTS;
	private String updateTS;
	private String fileConfigId;
	
	public FileSnapshotDto toDto() {
		FileSnapshotDto fileState = new FileSnapshotDto();
		fileState.setFileId(this.fileId);
		fileState.setFileName(this.fileName);
		fileState.setFileStatus(FileStatus.valueOf(this.fileStatus));
		fileState.setCreationTS(this.creationTS);
		fileState.setUpdateTS(this.updateTS);
		fileState.setFileConfigId(this.fileConfigId);
		
		return fileState;
	}

}
