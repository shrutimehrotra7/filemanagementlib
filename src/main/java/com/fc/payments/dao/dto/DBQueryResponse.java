package com.fc.payments.dao.dto;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import lombok.Data;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
public class DBQueryResponse {
	private ResultSet rs;
	private Connection con;
	private Statement stmt;
}
