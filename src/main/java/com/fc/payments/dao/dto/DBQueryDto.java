package com.fc.payments.dao.dto;

import com.fc.payments.file.template.dto.Footer;
import com.fc.payments.file.template.dto.Header;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class DBQueryDto {
	private String host;
	private String userName;
	private String password;
	private String dbName;
	private String[] tableName;
	private String[] columns;
	private String whereClause;
	private Header batchHeader;
	private Footer batchFooter;
}
