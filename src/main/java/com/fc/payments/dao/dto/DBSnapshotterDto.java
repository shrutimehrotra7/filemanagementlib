package com.fc.payments.dao.dto;

import java.util.Map;

import lombok.ToString;

/**
 * 
 * @author shruti.mehrotra
 *
 */

@ToString
public class DBSnapshotterDto extends DBQueryDto {

	private Map<String, String> columnToUpdate;

	public Map<String, String> getColumnToUpdate() {
		return columnToUpdate;
	}

	public void setColumnToUpdate(Map<String, String> columnToUpdate) {
		this.columnToUpdate = columnToUpdate;
	}
	
	public DBSnapshotterDto(DBQueryDto dto) {
		super.setBatchFooter(dto.getBatchFooter());
		super.setBatchHeader(dto.getBatchHeader());
		super.setColumns(dto.getColumns());
		super.setDbName(dto.getDbName());
		super.setHost(dto.getHost());
		super.setPassword(dto.getPassword());
		super.setTableName(dto.getTableName());
		super.setUserName(dto.getUserName());
		super.setWhereClause(dto.getWhereClause());
	}

}
