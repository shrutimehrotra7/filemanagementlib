package com.fc.payments.dao.mapper;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fc.payments.dao.entity.FileConfigSnapshotEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 03-Apr-2017
 */
public interface IFileConfigSnapshotEntityMapper {
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "transactionManager")
	public void insert(FileConfigSnapshotEntity fileConfigSnapshot);
	
	@Transactional(value = "transactionManager")
	public List<FileConfigSnapshotEntity> getAllFileConfigSnapshots();

	@Transactional(value = "transactionManager")
	public List<FileConfigSnapshotEntity> getByFileConfigType(String fileConfigType);
	
	@Transactional(value = "transactionManager")
	public FileConfigSnapshotEntity getByFileConfigId(String fileConfigId);
}