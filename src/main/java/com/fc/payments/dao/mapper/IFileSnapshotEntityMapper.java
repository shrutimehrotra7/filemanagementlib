package com.fc.payments.dao.mapper;

import java.util.List;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fc.payments.dao.entity.FileSnapshotEntity;

/**
 * 
 * @author Shruti Mehrotra (shruti.mehrotra@freecharge.com)
 * created on 17-Mar-2017
 */
public interface IFileSnapshotEntityMapper {
	@Transactional(propagation = Propagation.REQUIRES_NEW, value = "transactionManager")
	public void insert(FileSnapshotEntity fileSnapshot);
	
	@Transactional(value = "transactionManager")
	public List<FileSnapshotEntity> getAllFileSnapshots();

//	public List<FileSnapshotEntity> getByFileType(String fileType);
	
	@Transactional(value = "transactionManager")
	public FileSnapshotEntity getByFileId(String fileId);
	
	@Transactional(value = "transactionManager")
	public void updateStatusByFileId(FileSnapshotEntity entity);
	
	@Transactional(value = "transactionManager")
	public void updateFileNameAndStatusByFileId(FileSnapshotEntity entity);
}