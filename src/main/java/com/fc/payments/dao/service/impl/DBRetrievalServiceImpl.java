package com.fc.payments.dao.service.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Component;

import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBQueryResponse;
import com.fc.payments.dao.service.IDBRetrievalService;
import com.fc.payments.exceptions.QueryExecutionException;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("DBRetrievalServiceImpl")
public class DBRetrievalServiceImpl extends DBConnectionService implements IDBRetrievalService {

	private Connection con;
	private Statement stmt;
	private ResultSet rs;

	@Override
	public DBQueryResponse retrieveData(DBQueryDto dto, String selectQuery) {
		DBQueryResponse response = new DBQueryResponse();
		con = getConnection(dto);
		stmt = getStatement(con);
		// executing SELECT query
		try {
			rs = stmt.executeQuery(selectQuery);
			/*while (rs.next()) {
				for (int i = 0; i < dto.getColumns().length; i++) {
					System.out.println("Record : " + rs.getString(i));
				}
			}*/
		} catch (SQLException e) {
			throw new QueryExecutionException("Unable to execute the following query: " + selectQuery, e);
		} 
		response.setCon(con);
		response.setStmt(stmt);
		response.setRs(rs);
		return response;

	}

}
