package com.fc.payments.dao.service;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import com.fc.payments.dao.dto.DBSnapshotterDto;

public interface IDBUpdateService {
	public void update(DBSnapshotterDto dto);
}
