package com.fc.payments.dao.service;
/**
 * 
 * @author shruti.mehrotra
 *
 */

import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBQueryResponse;

public interface IDBRetrievalService {
	public DBQueryResponse retrieveData(DBQueryDto dto, String selectQuery);
}
