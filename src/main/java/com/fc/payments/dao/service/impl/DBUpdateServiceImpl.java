package com.fc.payments.dao.service.impl;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.stereotype.Component;

import com.fc.payments.dao.DBUtil;
import com.fc.payments.dao.dto.DBSnapshotterDto;
import com.fc.payments.dao.service.IDBUpdateService;
import com.fc.payments.exceptions.QueryExecutionException;

/**
 * 
 * @author shruti.mehrotra
 *
 */
@Component("DBUpdateServiceImpl")
public class DBUpdateServiceImpl extends DBConnectionService implements IDBUpdateService {

	private Connection con;
	private Statement stmt;

	@Override
	public void update(DBSnapshotterDto dto) {
		con = getConnection(dto);
		stmt = getStatement(con);
		// executing SELECT query
		String query = DBUtil.getUpdateQueryByDto(dto);
		try {
			stmt.executeUpdate(query);
		} catch (SQLException e) {
			throw new QueryExecutionException("Unable to execute the following query: " + query, e);
		} finally {
			// close connection, stmt
			try {
				con.close();
			} catch (SQLException se) {
				/* can't do anything */
			}
			try {
				stmt.close();
			} catch (SQLException se) {
				/* can't do anything */
			}
		}
	}

}
