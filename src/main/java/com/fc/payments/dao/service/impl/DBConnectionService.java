package com.fc.payments.dao.service.impl;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.fc.payments.dao.DBUtil;
import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.exceptions.DatabaseConnectionException;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class DBConnectionService {

	public Connection getConnection(DBQueryDto dto) {
		Connection con;
		String url = DBUtil.getDBUrl(dto.getHost(), dto.getDbName());
		String password = dto.getPassword();
		String userName = dto.getUserName();
		try {
			// opening database connection to MySQL server
			con = DriverManager.getConnection(url, userName, password);
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException(
					"Cannot connect to the database with the following config: " + dto.toString(), sqlEx);
		}
		return con;
	}
	
	public Statement getStatement(Connection con) {
		Statement stmt = null;
		try {
			// getting Statement object to execute query
			stmt = con.createStatement();
		} catch (SQLException sqlEx) {
			throw new DatabaseConnectionException(
					"Cannot connect to the database: " , sqlEx);
		}

		return stmt;
	}
}
