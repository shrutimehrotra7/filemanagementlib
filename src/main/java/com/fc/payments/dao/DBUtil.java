package com.fc.payments.dao;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.fc.payments.constants.DBConstants;
import com.fc.payments.dao.dto.DBQueryDto;
import com.fc.payments.dao.dto.DBSnapshotterDto;
import com.fc.payments.model.FileSnapshotAndConfigResponse;
import com.fc.payments.util.AppUtil;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class DBUtil {
	public static String getDBUrl(String host, int port, String dbName) {
		StringBuilder url = new StringBuilder();
		
		url.append("jdbc:mysql://")
			.append(host)
			.append(":")
			.append(port)
			.append("/")
			.append(dbName)
			.append("?autoReconnect=true");
		return url.toString();
	}
	
	public static String getDBUrl(String host, String dbName) {
		return getDBUrl(host, 3306, dbName);
	}
	
	public static String getSelectQueryByDto(DBQueryDto dto) {
		StringBuilder selectQuery = new StringBuilder();
		selectQuery.append("SELECT ");
		if(dto.getColumns()!= null &&
				dto.getColumns().length > 0) {
			int count = 0;
			for (String column: dto.getColumns()) {
				selectQuery.append(column);
				count ++;
				if (count != dto.getColumns().length) {
					selectQuery.append(",");
				}
			}
		} else {
			selectQuery.append("*");
		}
		
		selectQuery.append(" FROM ");
		appendTableNamesToQuery(selectQuery, dto.getTableName());
		
		if (!StringUtils.isEmpty(dto.getWhereClause())) {
			if (!dto.getWhereClause().toUpperCase().contains("WHERE")) {
				selectQuery.append(" WHERE ");
			}
			selectQuery.append(dto.getWhereClause());
		}
		return selectQuery.toString();
	}
	
	public static String getUpdateQueryByDto(DBSnapshotterDto dto) {
		StringBuilder updateQuery = new StringBuilder();
		updateQuery.append("UPDATE ");
		appendTableNamesToQuery(updateQuery, dto.getTableName());
		updateQuery.append(" SET ");

		int entrySetCount = 0;
		for (Map.Entry<String, String> entry: dto.getColumnToUpdate().entrySet()) {
			updateQuery.append(entry.getKey())
			.append("=")
			.append(AppUtil.quoteString(entry.getValue()));
			entrySetCount ++;
			if (entrySetCount != dto.getColumnToUpdate().size()) {
				updateQuery.append(",");
			}
		}
		
		if (!StringUtils.isEmpty(dto.getWhereClause())) {
			if (!dto.getWhereClause().toUpperCase().contains("WHERE")) {
				updateQuery.append(" WHERE ");
			}
			updateQuery.append(" ")
						.append(dto.getWhereClause());
		}
		return updateQuery.toString();
	}
	
	public static void appendTableNamesToQuery(StringBuilder query, String[] tableNames) {
		for (int i = 0; i< tableNames.length; i++) {
			query.append(tableNames[i]).append(" ");
			
			if (i != tableNames.length -1) {
				query.append(", ");
			}
		}
	}
	
	public static String replaceParametersInWhereClause(String whereClause, FileSnapshotAndConfigResponse response) {
		if (!StringUtils.isEmpty(whereClause)) {
			
			Map<String, String> allowedParamsInQuery = getAllowedConfParamsInQuery(response);
			
			String[] taggedStrArray = AppUtil.getStringsBetweenTags(whereClause);
			
			if (taggedStrArray != null && taggedStrArray.length > 0) {
				for (String taggedStr: taggedStrArray) {
					for(Map.Entry<String, String> entry: allowedParamsInQuery.entrySet()) {
						if (taggedStr.equals(entry.getKey())) {
							whereClause = StringUtils.replace(whereClause, AppUtil.taggenateString(taggedStr), AppUtil.quoteString(entry.getValue()));
						}
					}
				}
			}
		}
		return whereClause;
	}
	
	public static Map<String, String> getAllowedConfParamsInQuery(FileSnapshotAndConfigResponse response) {
		Map<String, String> allowedParamsInQuery = new HashMap<>();
		allowedParamsInQuery.put(DBConstants.FILE_ID, response.getFileSnapshotDto().getFileId());
		allowedParamsInQuery.put(DBConstants.FILE_STATUS, response.getFileSnapshotDto().getFileStatus().name());
		allowedParamsInQuery.put(DBConstants.FILE_TYPE, response.getFileConfigSnapshotDto().getFileConfigType().name());
		return allowedParamsInQuery;
	}
}