package com.fc.payments.dao.config;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.fc.payments.dao.DBUtil;

/**
 * Created by shruti.mehrotra on 19/5/16.
 */
@Configuration
public class DatabaseConfiguration {

	@Value("${database.host}")
	private String host;

	@Value("${database.port}")
	private int port;
	
	@Value("${database.name}")
	private String dbName;

	@Value("${database.userName}")
	private String userName;
	
	@Value("${database.password}")
	private String password;

	@Value("${database.driverClassName}")
	private String driverClassName;

	@Value("${datasource.initialSize}")
	private Integer dataSourceInitialSize;

	@Value("${datasource.maxActive}")
	private Integer dataSourceMaxActive;

	@Bean(name = "mysqlDatasource")
	@Scope(value = BeanDefinition.SCOPE_PROTOTYPE)
	public BasicDataSource getMysqlDatasource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setDriverClassName(driverClassName);
		dataSource.setDefaultAutoCommit(true);
		dataSource.setPoolPreparedStatements(false);
		dataSource.setInitialSize(dataSourceInitialSize);
		dataSource.setMaxActive(dataSourceMaxActive);
		dataSource.setMaxWait(8 * 10800000);
		dataSource.setMaxIdle(200);
		dataSource.setMinIdle(100);
		dataSource.setTimeBetweenEvictionRunsMillis(34000);
		dataSource.setMinEvictableIdleTimeMillis(55000);
		dataSource.setValidationQuery("SELECT 1");
		dataSource.setTestOnBorrow(true);
		dataSource.setTestWhileIdle(true);
		
		
		dataSource.setUrl(DBUtil.getDBUrl(host, port, dbName));
		dataSource.setUsername(userName);
		dataSource.setPassword(password);
		
		
		return dataSource;
	}
}
