package com.fc.payments.file.template;

import com.fc.payments.file.template.dto.Batch;
import com.fc.payments.file.template.dto.Body;
import com.fc.payments.file.template.dto.Column;
import com.fc.payments.file.template.dto.DBConfig;
import com.fc.payments.file.template.dto.FileTemplate;
import com.google.gson.Gson;

/**
 * 
 * @author shruti.mehrotra
 *
 */
public class TemplateGeneratorService {
	public static void main(String[] args) {
		FileTemplate fileTemplate = new FileTemplate();
		DBConfig dbConfig = new DBConfig();
		dbConfig.setDbName("klickpay");
		dbConfig.setHost("localhost");
		fileTemplate.setDbConfig(dbConfig );
		Batch batch1 = new Batch();
		Column col1 = new Column();
		col1.setId("col1");
		col1.setName("fileId");
		Column col2 = new Column();
		col2.setId("col2");
		col2.setName("fileName");
		Column col3 = new Column();
		col3.setId("col3");
		col3.setName("status");
		Column[] batchHrColumns = {col1, col2, col3};
		batch1.getBatchHeader().setColumns(batchHrColumns);
		Body body1 = new Body();
		Column[] columns1 = {col1,col2,col3};
		body1.setColumns(columns1 );
		String[] tableName = {"txn"};
		body1.setTableName(tableName );
		body1.setWhereClause("WHERE fileId=1");
		body1.setOrderByClause("ORDER BY STATUS desc");
		batch1.setBody(body1 );
		
		Batch batch2 = new Batch();
		Column[] batch2HrColumns = {col1, col2, col3};
		batch2.getBatchHeader().setColumns(batch2HrColumns);
		
		
		Batch[] batches = {batch1, batch2};
		fileTemplate.setBatches(batches );
		fileTemplate.setFileDelimiter(',');
		System.out.println(new Gson().toJson(fileTemplate));
		
		System.out.println(fileTemplate.toString());
	}
}
